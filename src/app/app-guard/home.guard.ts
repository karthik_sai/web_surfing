import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
 
@Injectable()
export class HomeGuard implements CanActivate {
  constructor(private router : Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean {
      console.log(localStorage.getItem('userToken'));
      if (localStorage.getItem('userToken') == null)
      return true;
      this.router.navigate(['/dashboard']);
      return false;
  }
}
