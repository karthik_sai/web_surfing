import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppAuthModule } from './app-auth/app-auth.module';
import { AppLayoutModule } from './app-layout/app-layout.module';
import { AuthGuard } from './app-guard/auth.guard';
import { HomeGuard } from './app-guard/home.guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppInterceptor } from './app-guard/app-interceptor';
import { NotifierModule } from 'angular-notifier';
import { NgHttpLoaderModule } from 'ng-http-loader';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppAuthModule,
    AppLayoutModule,
    ReactiveFormsModule,
    NotifierModule.withConfig( {
      position: {
        horizontal: {
          position: 'right',
          distance: 12
        },
        vertical: {
          position: 'top',
          distance: 12,
          gap: 10
        }
      },
      behaviour: {
        autoHide: 3000,
        onClick: false,
        onMouseover: 'pauseAutoHide',
        showDismissButton: true
      }
    }),
    NgHttpLoaderModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [HomeGuard, AuthGuard,
              {
                provide: HTTP_INTERCEPTORS,
                useClass: AppInterceptor,
                multi: true
              }    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
