"use strict";

const express = require("express"), router = express.Router(), fs = require("fs");

fs.readdirSync(__dirname).forEach(function(file) {
    const filename = file.split(".")[0];
    if(filename != "app-routes"){
        router.use("/" + filename, require("./" + file));
    }
});

module.exports = router;
