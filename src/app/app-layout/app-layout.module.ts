import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { layoutRoutes } from './layout.routes';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardService } from './dashboard.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      layoutRoutes
    ),
  ],
  providers: [DashboardService],
  declarations: [DashboardComponent, NavbarComponent]
})
export class AppLayoutModule { }
