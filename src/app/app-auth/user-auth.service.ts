import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

import { Headers } from '@angular/http';
import { map } from "rxjs/operators";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UserAuthService {

  result: any;

  programs: any;

  srvcshtName;
  filterType;
  page;
  deffered_flag;

  csrfToken: any;

  constructor(private _http: HttpClient) { }

  userSignup(user) {
    const reqHeader = new HttpHeaders({ 'signup':'true' });
    return this._http.post(environment.apiEndpoint+"/signup", user, { headers: reqHeader }).pipe(
      map(result => {
        return result;
      }
      )
    );
  }

  login(user) {
    const reqHeader = new HttpHeaders({ 'login':'true' });
    return this._http.post(environment.apiEndpoint + '/login', user, { headers: reqHeader }).pipe(
      map(result => {
        return result;
      }
      ));
  }

}
