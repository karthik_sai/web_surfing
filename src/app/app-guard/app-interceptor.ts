import { HttpInterceptor, HttpRequest, HttpHandler, HttpUserEvent, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/do';
import { environment } from '../../environments/environment';
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { SpinnerVisibilityService } from "ng-http-loader";
import { NotifierService } from "angular-notifier";
 
@Injectable()
export class AppInterceptor implements HttpInterceptor {

    private readonly notifier: NotifierService;

    constructor(
        private router: Router,
        private spinner: SpinnerVisibilityService,
        notifierService: NotifierService
    ) { 
        this.notifier = notifierService;
    }
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.spinner.show();
        if (req.headers.get('signup') == "true") {
            return next.handle(req)
            .do(
                succ => {
                    this.spinner.hide();
                },
                err => {
                    this.spinner.hide();
                    this.notifier.notify( 'error', err.error.message );
                    console.log("err_inter",err);
                    if (err.status === 403)
                        this.router.navigateByUrl('/home');
                }
            );
        } else if (req.headers.get('login') == "true") {
            console.log("login");
            
            const clonedreq = req.clone({
                headers: req.headers.set("authorization", environment.clientAuthHeader)
            });
            return next.handle(clonedreq)
                .do(
                    succ => { 
                        this.spinner.hide();
                    },
                    err => {
                        this.spinner.hide();
                        this.notifier.notify( 'error', err.error.message );
                        if (err.status === 403)
                            this.router.navigateByUrl('/home');
                    }
                );
        } else if (localStorage.getItem('userToken') != null) {
            const clonedreq = req.clone({
                headers: req.headers.set("x-access-token", localStorage.getItem('userToken'))
            });
            this.spinner.hide();
            return next.handle(clonedreq)
                .do(
                succ => { 
                    this.spinner.hide();
                },
                err => {
                    this.spinner.hide();
                    this.notifier.notify( 'error', err.error.message );
                    if (err.status === 403) {
                        localStorage.removeItem('userToken');
                        this.router.navigateByUrl('/home');
                    }
                }
                );
        } else {
            this.spinner.hide();
            this.router.navigateByUrl('/home');
        }

    }
}
