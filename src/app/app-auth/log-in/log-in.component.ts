import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserAuthService } from '../user-auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  loginForm: any;

  constructor(
    private userAuthService: UserAuthService,
    private router : Router
  ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required)
    });
  }

  onSubmit() {
    let req_body = {};
    req_body = this.loginForm.value;
    this.userAuthService.login(req_body).subscribe((response:any)=>{
      if(response.x_access_token){
        localStorage.setItem('userToken',response.x_access_token);
        this.router.navigate(['/dashboard']);
      }
    });
  }

}
