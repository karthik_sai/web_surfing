import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

import { map } from "rxjs/operators";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class DashboardService {

  result: any;

  programs: any;

  srvcshtName;
  filterType;
  page;
  deffered_flag;

  csrfToken: any;

  constructor(private _http: HttpClient) { }

  getDashboardUser() {
    return this._http.get(environment.apiEndpoint+"/users").pipe(
      map(result => {
        return result;
      }
      )
    );
  }

  logout() {
    return this._http.post(environment.apiEndpoint + '/logout', {}).pipe(
      map(result => {
        return result;
      }
      ));
  }

}
