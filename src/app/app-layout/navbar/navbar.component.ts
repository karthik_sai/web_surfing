import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private dashboardService: DashboardService,
    private router : Router
  ) { }

  ngOnInit() {
  }

  logout() {
    this.dashboardService.logout().subscribe((response:any)=>{
      localStorage.removeItem('userToken');
      this.router.navigate(['/home']);
    });
  }

}
