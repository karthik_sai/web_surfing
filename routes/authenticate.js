"use strict";

const config = require("../config/config"),
      jwt = require('jsonwebtoken'),
      bcrypt = require('bcrypt-nodejs'),
      express = require("express");

const secretKey = config.secretKey;
const User  = require("../models/user");
const API = express.Router();

API.post("/signup", function (request, response) {
    
  const emailExpression = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const passwordExpression = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
  if (request.body.email && request.body.username && request.body.password) {

      if (emailExpression.test(request.body.email) === true) {
        if (passwordExpression.test(request.body.password)){
          User.findOne({ email: request.body.email }, function (err, user) {
            if (user) {
              response.status(500).send({ message: "Email already registered" });
            } else {
              User.findOne({ username: request.body.username }, function (err, user) {
                if (user) {
                  response.status(500).send({ message: "Username already registered" });
                } else {
                  var user = new User({
                    username: request.body.username,
                    email: request.body.email,
                    password: request.body.password
                  });
                  user.save(function (err, result) {
                      // Check for errors
                      if (err) {
                          if (err.message.indexOf("$username") > -1) {
                              response.status(500).send({ message: "Username already registered" });
                          } else if (err.message.indexOf("$email") > -1){
                              response.status(500).send({ message: "Already email taken" });
                          }
                          console.log("error",err.message);
                          // response.send(err);
                          return;
                      }
                      // Send status message on success
                      response.status(200).send({ message: "Sign Up Successfull" });
                  }); 
                }
              });
            }
          });
        } else{
            response.status(400).send({ message: "Not a valid password! Password should contain atleast 8 characters with atleast one uppercase letter and a number " });
        }
    } else {
        response.status(400).send({ message: "Not a valid email" });
    }

  } else {
    response.status(400).json({ message: "UserValues cannot be Null" })
  }

});

API.post("/login/", function(request, response){

  // console.log(request.headers.authorization);
  // console.log(new Buffer(config.client_id + " _ " + config.client_secret).toString('base64'));

  const authHeader = request.headers['authorization'];
  const authHeaderCheck = new Buffer(config.client_id + "&" + config.client_secret).toString('base64');
  console.log(authHeader,authHeaderCheck);
  if (authHeader === authHeaderCheck) {
    if (request.body.username && request.body.password) {

      User.findOne({ username: request.body.username }, function (err, user) {

        if (err) {
          throw err;
        }

        if (!user) {
          response.status(403).json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {
          // console.log(user);
          // check if password matches
          if ((User.schema.methods.comparePassword(request.body.password, user.password)) === false) {
            response.status(403).json({ success: false, message: 'Authentication failed. Wrong password.' });
          } else if (user.x_access_token !== "") {
            
            if (new Date() < new Date((jwt.decode(user.x_access_token).exp) * 1000)){
              // return the information including token as JSON
              response.json({
                user_id: user._id,
                user_name: user.fullname,
                verified: user.verified,
                x_access_token: user.x_access_token,
                expires_in: Math.ceil(Math.abs(new Date((jwt.decode(user.x_access_token).exp) * 1000).getTime() - new Date().getTime()) / (1000))
              });
            } else{
              
              // create a new token
              var token = jwt.sign({ user: user._id }, secretKey, {
                expiresIn: 60*60*24 // expires in 24 hours
              });

              const obj = {
                x_access_token: token,
              }

              User.findByIdAndUpdate(user._id, obj, function (err, result) {
                if (err) {
                  response.status(500).send(err);
                }
                if (result) {
                  // return the information including token as JSON
                  response.json({
                    user_id: user._id,
                    user_name: user.fullname,
                    verified: user.verified,
                    x_access_token: token,
                    expires_in: Math.ceil(Math.abs(new Date((jwt.decode(token).exp) * 1000).getTime() - new Date().getTime()) / (1000))
                  });
                }
              });

            }
          } else {

              // if user is found and password is right
              // create a token
              var token = jwt.sign({ user: user._id }, secretKey, {
                expiresIn: 60 * 60 * 24 // expires in 24 hours
              });

              const obj = {
                x_access_token: token
              }

              User.findByIdAndUpdate(user._id, obj, function (err, result) {
                if (err) {
                  response.status(500).send(err);
                }
                if (result) {
                  // return the information including token as JSON
                  response.json({
                    user_id: user._id,
                    user_name: user.fullname,
                    verified: user.verified,
                    x_access_token: token,
                    expires_in: Math.ceil(Math.abs(new Date((jwt.decode(token).exp) * 1000).getTime() - new Date().getTime()) / (1000))
                  });
                }
              });
            }
          }
        });
      }
      else {
        response.status(400).json({ message: "UserValues cannot be Null" })
      }
  } else {
    // if there is no authorization header
    // return an error
    return response.status(403).send({
      message: 'Not Authorized.'
    });
  }
});

// route middleware to verify a token
API.use(function (req, res, next) {


  // check header or url parameters or post parameters for token
  var token = req.headers ['x-access-token'];

  // console.log(token);
  // decode token
  if (token) {
    User.findOne({ x_access_token: token }, function (err, user) {
      
      if (err) {
        throw err;
      }
      if (!user) {
        res.status(403).json({ success: false, message: 'Failed to authenticate.' });
      }

      else if (user) {
        jwt.verify(token, config.secretKey, function (err, decoded) {
          if (err) {
            return res.status(403).json({ success: false, message: 'Failed to authenticate.' });
          } else {
            // if everything is good, save to request for use in other routes
            req.decoded = decoded;
            next();
          }
        });
      }

    });
  } else {

    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });

  }
});

API.post("/logout", function (request, response) {

  const authToken = request.headers['x-access-token'];

  User.findOne({ x_access_token: authToken }, function (err, user) {

      if (err) {
          response.status(500).send(err);
      }
      if (!user) {
          response.status(200).json({});
      }
      else if (user) {
          const obj = {
              x_access_token: ""
          }
          User.findByIdAndUpdate(user._id, obj, function (err, result) {
              if (err) {
                  response.status(500).send(err);
              }
              if (result) {
                  response.status(200).json({});
              }
          });
      }
  });

});

// Export the board routes
module.exports = API;
