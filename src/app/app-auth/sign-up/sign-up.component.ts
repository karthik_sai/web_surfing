import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserAuthService } from '../user-auth.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  private readonly notifier: NotifierService;

  signupForm: any;

  constructor(
    private userAuthService: UserAuthService,
    notifierService: NotifierService
  ) {
    this.notifier = notifierService;
   }

  ngOnInit() {
    this.signupForm = new FormGroup({
      email: new FormControl('',Validators.required),
      username: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required)
    });
  }

  onSubmit() {
    let req_body = {};
    req_body = this.signupForm.value;
    this.userAuthService.userSignup(req_body).subscribe((response:any)=>{
      this.reset();
      this.notifier.notify( 'success', response.message );
    });
  }

  reset(){
    this.signupForm.setValue({
      email: '',
      username: '',
      password: ''
    });
  }

}
