"use strict";
/**
 * Emvironment settings and Configurations
 * =============================================================================
 * This is the file where all the Configurations for the server.js and its
 * related files uses. You can add additional Configurations here.
 *
 * @type {Object}
 */
module.exports = {
  "database" : "mongodb://localhost:27017/web_surf",
  "databaseOptions" : {
    user: 'web_surf_admin',
    pass: 'Qwerty12',
    useMongoClient: true 
  },
  "client_id": "webSurfDesk",
  "client_secret": "webSurf@0!8",
  "port" : process.env.PORT || 8040,
  // Useful if there is any jwt implementations
  "tempKey" : "web_surf",
  "secretKey"  : "webSurf@2018"
};