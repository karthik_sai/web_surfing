import { Routes } from '@angular/router';
import { AuthComponent } from './app-auth/auth/auth.component';
import { HomeGuard } from './app-guard/home.guard';

export const appRoutes: Routes = [
	{ path: '', redirectTo: 'home', pathMatch: 'full' },
	{ path: 'home', component: AuthComponent, canActivate:[HomeGuard] }
];
