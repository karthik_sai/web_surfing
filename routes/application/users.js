"use strict";
const config = require("../../config/config"),
      express = require("express"),
      jwt = require('jsonwebtoken');

const secretKey = config.secretKey;
var bcrypt = require('bcrypt-nodejs');

const User  = require("../../models/user");

const API = express.Router();

API.get("/", function(request, response) {
  var decoded = jwt.verify(request.headers ['x-access-token'], config.secretKey);
  User.findById(decoded.user, function(err, user) {
    if (user) {
      const returningUser = {
        _id: user._id,
        username: user.username,
        email: user.email
      }
      response.send(returningUser);
    } else {
      response.status(404).send("Not Found");
    }
  });
});

module.exports = API;
