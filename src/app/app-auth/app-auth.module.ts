import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogInComponent } from './log-in/log-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthComponent } from './auth/auth.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserAuthService } from './user-auth.service';
import { ModalModule, TabsModule } from 'ngx-bootstrap';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    TabsModule.forRoot(),
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  declarations: [LogInComponent, SignUpComponent, AuthComponent],
  providers: [ UserAuthService ],
  exports: [AuthComponent]
})
export class AppAuthModule { }
