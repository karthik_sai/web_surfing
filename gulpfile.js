const gulp = require("gulp"),spawn = require('child_process').spawn;;

gulp.task('node-server-start', function (cb) {
  spawn('nodemon', ['index.js'], {stdio: 'inherit'}) 
});
gulp.task('ng-serve', function (cb) {
  spawn('ng', ['serve'], {stdio: 'inherit'}) 
});
gulp.task('start', ['node-server-start', 'ng-serve'], function () {
});