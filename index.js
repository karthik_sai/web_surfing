"use strict";

const express = require("express"),
  bodyParser = require("body-parser"),
  busboyBodyParser = require("busboy-body-parser"),
  morgan = require("morgan"),
  jwt = require('jsonwebtoken'),
  config = require("./config/config"),
  mongoose = require("mongoose"),
  flash = require('connect-flash'),
  cookieParser = require('cookie-parser'),
  session = require('express-session'),
  cors = require("cors");


const app = express();
app.use(cors());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

mongoose.Promise = global.Promise;

mongoose.connect(config.database, config.databaseOptions, function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log("::connected to database::");
  }
});

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: true })); // Allow any type of encoding
app.use(bodyParser.json());
app.use(cookieParser());
app.use(busboyBodyParser());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(flash());

app.use("/webSurf/api", require("./routes/authenticate"));
app.use("/webSurf/api", require("./routes/application/app-routes"));

app.use(function (req, res, next) {
  res.status(404);

  if (req.accepts("json")) {
    res.send({ error: "Not found" });
    return;
  }

  res.type("txt").send("Not found");
});

app.listen(config.port, function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log("::Running on port " + config.port + "::");
  }
});
