var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var Schema = mongoose.Schema;

var UserSchema = new Schema(
  {
    username: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    x_access_token: { type: String, default: ""},
    createdOn: { type: Date, default: Date.now }
});

UserSchema.pre("save", function(next) {
  const user = this;

   if(!user.isModified("password"))
    return next();
   bcrypt.hash(user.password, null, null, function (err, hash) {
     if (err) {
       return next(err);
     } else {
       user.password = hash;
       next();
     }
   });

});

UserSchema.methods.comparePassword = function (password, userPassword) {
  return bcrypt.compareSync(password, userPassword);
};

module.exports = mongoose.model("User", UserSchema);