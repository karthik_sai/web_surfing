import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private readonly notifier: NotifierService;

  user:any;

  constructor(
    private dashboardService: DashboardService,
    notifierService: NotifierService
  ) {
    this.notifier = notifierService;
   }

  ngOnInit() {
    this.dashboardService.getDashboardUser().subscribe((response:any)=>{
      this.user = response;
    });
  }

}
