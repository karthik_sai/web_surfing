import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from '../app-guard/auth.guard';

export const layoutRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent, canActivate:[AuthGuard]}
];
